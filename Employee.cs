﻿namespace ConsoleApp1
{
    internal class Employee
    {
        internal int Id { get; set; }
        internal string Name { get; set; } = string.Empty;
        internal List<Vacation> vacations { get; set; } = new List<Vacation>();
    }
}
