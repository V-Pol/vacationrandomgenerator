﻿namespace ConsoleApp1
{
    internal class DataInitizializer
    {
        const int employeeNumber = 6;

        /// <summary>
        /// Инициализация списка сотрудников
        /// </summary>
        /// <returns>Возвращает список сотрудников</returns>
        internal List<Employee> InitizializeEmployees() 
        {
            List<Employee> Employees = new List<Employee>();
            for (int i = 0; i < employeeNumber; i++)
            {
                Employees.Add(new Employee() { Id = i });
            }
            Employees[0].Name = "Иванов Иван Иванович";
            Employees[1].Name = "Петров Петр Петрович";
            Employees[2].Name = "Юлина Юлия Юлиановна";
            Employees[3].Name = "Сидоров Сидор Сидорович";
            Employees[4].Name = "Павлов Павел Павлович";
            Employees[5].Name = "Георгиев Георг Георгиевич";
            return Employees;
        }
    }
}
