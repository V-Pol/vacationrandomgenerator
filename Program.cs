﻿using System.Globalization;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            const int BaseVacationCount = 28;
            const int requiredPartVacation = 14;
            List<int> have14DaysVacationParts = new List<int>();
            var cultureInfo = new CultureInfo("En-en");
            var WorkingDays = cultureInfo.DateTimeFormat.DayNames[1..6];
            int[] vacationSteps = { 7, 14 };
            const int dayBetweenVacations = 3;
            int range = cultureInfo.Calendar.GetDaysInYear(DateTime.Now.Year);
            DateTime startDayOfYear = new DateTime(DateTime.Now.Year, 1, 1);
            var employees = new DataInitizializer().InitizializeEmployees();// Список отпусков сотрудников
            List<Vacation> vacations = new List<Vacation>();

            foreach (var employee in employees)
            {
                Random gen = new Random();
                int vacationCount = BaseVacationCount;
                while (vacationCount > 0)
                {
                    int vacationIndex = gen.Next(vacationSteps.Length);
                    int difference = vacationSteps[vacationIndex];
                    if (!have14DaysVacationParts.Contains(employee.Id))
                        difference = requiredPartVacation;
                    if (vacationCount <= vacationSteps[0])
                        difference = vacationSteps[0];
                    var startDate = startDayOfYear.AddDays(gen.Next(range - difference));
                    var endDate = startDate.AddDays(difference);

                    if (WorkingDays.Contains(startDate.DayOfWeek.ToString()))
                    {
                        // Проверка условий по отпуску
                        bool CanCreateVacation = false;
                        bool existStart = false;
                        bool existEnd = false;
                        if (!vacations.Any(element => checkDateRangesIntersection(element.startDate, element.endDate)))
                        {
                            if (!vacations.Any(element => 
                                checkDateRangesIntersection(element.startDate.AddDays(dayBetweenVacations), element.endDate.AddDays(dayBetweenVacations))))
                            {
                                existStart = employee.vacations.Any(element => checkDateRangesIntersection(element.startDate.AddMonths(1), element.endDate.AddMonths(1)));
                                existEnd = employee.vacations.Any(element => checkDateRangesIntersection(element.startDate.AddMonths(-1), element.endDate.AddMonths(-1)));
                                if (!existStart || !existEnd)
                                {
                                    CanCreateVacation = true;
                                    if (!have14DaysVacationParts.Contains(employee.Id))
                                        have14DaysVacationParts.Add(employee.Id);
                                }
                            }
                        }
                        if (CanCreateVacation)
                        {
                            var vacation = new Vacation { startDate = startDate, endDate = endDate };
                            employee.vacations.Add(vacation);
                            vacations.Add(vacation);

                            vacationCount -= difference;
                        }
                    }
                    //Проверка диапазонов дат на пересечение
                    bool checkDateRangesIntersection(DateTime elementsStartDate, DateTime elementsEndDate)
                    {
                        var start = elementsStartDate >= startDate && elementsStartDate <= endDate;
                        var end = elementsEndDate >= startDate && elementsEndDate <= endDate;
                        return start || end;
                    }
                }
            }

            foreach (var employee in employees)
            {
                var employeeVacations = employee.vacations.OrderBy(v => v.startDate);
                Console.WriteLine($"Периоды отпуска {employee.Name}: ");
                foreach (var vacation in employeeVacations)
                    Console.WriteLine($"{vacation.startDate:d} - {vacation.endDate:d}");
            }
            Console.ReadKey();
        }
    }
}